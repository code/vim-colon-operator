"
" colon_operator.vim: Select ranges and run colon commands on them, rather
" like the ! operator but for colon commands like :sort.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_colon_operator') || &compatible || v:version < 700
  finish
endif
let loaded_colon_operator = 1

" Set up mapping
nnoremap <expr> <Plug>(ColonOperator)
      \ colon_operator#()
