colon\_operator.vim
===================

This plugin provides a mapping target to use an Ex command as an operator, in a
way repeatable with the `.` (dot) operator.  This allows you to, for example,
`:sort` a text object like a paragraph, and then repeat it on another
paragraph.  Think of it as the `:` analogue to the `!` motion.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
